// Generated from C:\Enter here\documents\courses\91-92_2\Compiler\project\vandermonde\MAG1.g4 by ANTLR 4.
import java.util.List;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNSimulator;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;
import org.antlr.v4.runtime.tree.TerminalNode;

@SuppressWarnings({ "all", "warnings", "unchecked", "unused", "cast" })
public class MAG1Parser extends Parser {

    protected static final DFA[]                  _decisionToDFA;
    protected static final PredictionContextCache _sharedContextCache =
                                                                              new PredictionContextCache();
    public static final int
                                                  T__1                = 1, T__0 = 2, T_plus = 3, T_min = 4, T_star = 5, T_slash = 6, T_mode = 7, T_gt = 8,
                                                                      T_lt = 9, T_get = 10, T_let = 11, T_et = 12, T_net = 13, T_as = 14, T_and = 15, T_or = 16,
                                                                      T_not = 17, T_if = 18, T_else = 19, T_elif = 20, T_po = 21, T_pc = 22, T_cbo = 23, T_cbc = 24,
                                                                      T_cin = 25, T_cout = 26, T_int = 27, T_intvar = 28, T_float = 29, T_floatvar = 30,
                                                                      T_char = 31, T_charvar = 32, T_string = 33, T_stringvar = 34, T_comment = 35, T_semicolon = 36,
                                                                      T_throw = 37, T_try = 38, T_catch = 39, T_whitespace = 40, T_ID = 41;
    public static final String[]                  tokenNames          = {
                                                                      "<INVALID>", "'false'", "'true'", "'+'", "'-'", "'*'", "'/'", "'%'", "'>'",
                                                                      "'<'", "'>='", "'<='", "'=='", "'!='", "'='", "'&&'", "'||'", "'!'", "'if'",
                                                                      "'else'", "'elif'", "'('", "')'", "'{'", "'}'", "'>>'", "'<<'", "'int'",
                                                                      "T_intvar", "'float'", "T_floatvar", "'char'", "T_charvar", "'string'",
                                                                      "T_stringvar", "T_comment", "';'", "'throw'", "'try'", "'catch'", "T_whitespace",
                                                                      "T_ID"
                                                                      };
    public static final int
                                                  RULE_start          = 0, RULE_e = 1, RULE_e1 = 2, RULE_t = 3, RULE_t1 = 4, RULE_f = 5,
                                                                      RULE_no = 6, RULE_condition = 7, RULE_condition1 = 8, RULE_cond = 9, RULE_cond1 = 10,
                                                                      RULE_c = 11, RULE_bboolean = 12, RULE_relop = 13, RULE_ifSt = 14, RULE_elsePart = 15,
                                                                      RULE_statement = 16, RULE_s = 17, RULE_blockSt = 18, RULE_assignmentSt = 19,
                                                                      RULE_declarSt = 20, RULE_type = 21, RULE_iOSt = 22, RULE_inputSt = 23,
                                                                      RULE_outputSt = 24;
    public static final String[]                  ruleNames           = {
                                                                      "start", "e", "e1", "t", "t1", "f", "no", "condition", "condition1", "cond",
                                                                      "cond1", "c", "bboolean", "relop", "ifSt", "elsePart", "statement", "s",
                                                                      "blockSt", "assignmentSt", "declarSt", "type", "iOSt", "inputSt", "outputSt"
                                                                      };



    @Override
    public String getGrammarFileName() {
        return "MAG1.g4";
    }



    @Override
    public String[] getTokenNames() {
        return tokenNames;
    }



    @Override
    public String[] getRuleNames() {
        return ruleNames;
    }



    @Override
    public ATN getATN() {
        return _ATN;
    }

    public int blockNumber = 0;
    public int blockDepth  = 0;



    public MAG1Parser(TokenStream input) {
        super(input);
        _interp = new ParserATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
    }

    public static class StartContext extends ParserRuleContext {

        String type;



        public List<StatementContext> statement() {
            return getRuleContexts(StatementContext.class);
        }



        public StatementContext statement(int i) {
            return getRuleContext(StatementContext.class, i);
        }



        public StartContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_start;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterStart(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitStart(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitStart(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final StartContext start() throws RecognitionException {
        StartContext _localctx = new StartContext(_ctx, getState());
        enterRule(_localctx, 0, RULE_start);
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(53);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 0, _ctx);
                while (_alt != 2 && _alt != -1) {
                    if (_alt == 1) {
                        {
                            {
                                setState(50);
                                statement();
                            }
                        }
                    }
                    setState(55);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 0, _ctx);
                }
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class EContext extends ParserRuleContext {

        String type;



        public E1Context e1() {
            return getRuleContext(E1Context.class, 0);
        }



        public TContext t() {
            return getRuleContext(TContext.class, 0);
        }



        public EContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_e;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterE(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitE(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitE(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final EContext e() throws RecognitionException {
        EContext _localctx = new EContext(_ctx, getState());

        enterRule(_localctx, 2, RULE_e);
        try {
            setState(60);
            switch (getInterpreter().adaptivePredict(_input, 1, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(56);
                        t();
                        setState(57);
                        e1();
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(59);
                        t();
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class E1Context extends ParserRuleContext {

        String type;



        public E1Context e1() {
            return getRuleContext(E1Context.class, 0);
        }



        public TContext t() {
            return getRuleContext(TContext.class, 0);
        }



        public TerminalNode T_min() {
            return getToken(MAG1Parser.T_min, 0);
        }



        public TerminalNode T_plus() {
            return getToken(MAG1Parser.T_plus, 0);
        }



        public E1Context(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_e1;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterE1(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitE1(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitE1(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final E1Context e1() throws RecognitionException {
        E1Context _localctx = new E1Context(_ctx, getState());
        enterRule(_localctx, 4, RULE_e1);
        try {
            setState(74);
            switch (getInterpreter().adaptivePredict(_input, 2, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(62);
                        match(T_plus);
                        setState(63);
                        t();
                        setState(64);
                        e1();
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(66);
                        match(T_min);
                        setState(67);
                        t();
                        setState(68);
                        e1();
                    }
                    break;

                case 3:
                    enterOuterAlt(_localctx, 3);
                    {
                        setState(70);
                        match(T_min);
                        setState(71);
                        t();
                    }
                    break;

                case 4:
                    enterOuterAlt(_localctx, 4);
                    {
                        setState(72);
                        match(T_plus);
                        setState(73);
                        t();
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class TContext extends ParserRuleContext {

        String type;



        public FContext f() {
            return getRuleContext(FContext.class, 0);
        }



        public T1Context t1() {
            return getRuleContext(T1Context.class, 0);
        }



        public TContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_t;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterT(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitT(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitT(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final TContext t() throws RecognitionException {
        TContext _localctx = new TContext(_ctx, getState());
        enterRule(_localctx, 6, RULE_t);
        try {
            setState(80);
            switch (getInterpreter().adaptivePredict(_input, 3, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(76);
                        f();
                        setState(77);
                        t1();
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(79);
                        f();
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class T1Context extends ParserRuleContext {

        String type;



        public FContext f() {
            return getRuleContext(FContext.class, 0);
        }



        public T1Context t1() {
            return getRuleContext(T1Context.class, 0);
        }



        public TerminalNode T_slash() {
            return getToken(MAG1Parser.T_slash, 0);
        }



        public TerminalNode T_star() {
            return getToken(MAG1Parser.T_star, 0);
        }



        public TerminalNode T_mode() {
            return getToken(MAG1Parser.T_mode, 0);
        }



        public T1Context(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_t1;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterT1(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitT1(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitT1(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final T1Context t1() throws RecognitionException {
        T1Context _localctx = new T1Context(_ctx, getState());
        enterRule(_localctx, 8, RULE_t1);
        try {
            setState(94);
            switch (getInterpreter().adaptivePredict(_input, 4, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(82);
                        match(T_star);
                        setState(83);
                        f();
                        setState(84);
                        t1();
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(86);
                        match(T_slash);
                        setState(87);
                        f();
                        setState(88);
                        t1();
                    }
                    break;

                case 3:
                    enterOuterAlt(_localctx, 3);
                    {
                        setState(90);
                        match(T_star);
                        setState(91);
                        f();
                    }
                    break;

                case 4:
                    enterOuterAlt(_localctx, 4);
                    {
                        setState(92);
                        match(T_mode);
                        setState(93);
                        f();
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class FContext extends ParserRuleContext {

        String type;



        public EContext e() {
            return getRuleContext(EContext.class, 0);
        }



        public NoContext no() {
            return getRuleContext(NoContext.class, 0);
        }



        public TerminalNode T_ID() {
            return getToken(MAG1Parser.T_ID, 0);
        }



        public TerminalNode T_pc() {
            return getToken(MAG1Parser.T_pc, 0);
        }



        public TerminalNode T_po() {
            return getToken(MAG1Parser.T_po, 0);
        }



        public FContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_f;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterF(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitF(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitF(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final FContext f() throws RecognitionException {
        FContext _localctx = new FContext(_ctx, getState());
        enterRule(_localctx, 10, RULE_f);
        try {
            setState(102);
            switch (getInterpreter().adaptivePredict(_input, 5, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(96);
                        match(T_po);
                        setState(97);
                        e();
                        setState(98);
                        match(T_pc);
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(100);
                        match(T_ID);
                    }
                    break;

                case 3:
                    enterOuterAlt(_localctx, 3);
                    {
                        setState(101);
                        no();
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class NoContext extends ParserRuleContext {

        String type;



        public TerminalNode T_intvar() {
            return getToken(MAG1Parser.T_intvar, 0);
        }



        public TerminalNode T_floatvar() {
            return getToken(MAG1Parser.T_floatvar, 0);
        }



        public NoContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_no;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterNo(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitNo(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitNo(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final NoContext no() throws RecognitionException {
        NoContext _localctx = new NoContext(_ctx, getState());

        enterRule(_localctx, 12, RULE_no);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(104);
                _la = _input.LA(1);
                if ( !(_la == T_intvar || _la == T_floatvar)) {
                    _errHandler.recoverInline(this);
                }
                consume();
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ConditionContext extends ParserRuleContext {

        String type;



        public Condition1Context condition1() {
            return getRuleContext(Condition1Context.class, 0);
        }



        public CondContext cond() {
            return getRuleContext(CondContext.class, 0);
        }



        public ConditionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_condition;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterCondition(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitCondition(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitCondition(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final ConditionContext condition() throws RecognitionException {
        ConditionContext _localctx = new ConditionContext(_ctx, getState());
        enterRule(_localctx, 14, RULE_condition);
        try {
            setState(110);
            switch (getInterpreter().adaptivePredict(_input, 6, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(106);
                        cond();
                        setState(107);
                        condition1();
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(109);
                        cond();
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Condition1Context extends ParserRuleContext {

        String type;



        public Condition1Context condition1() {
            return getRuleContext(Condition1Context.class, 0);
        }



        public TerminalNode T_or() {
            return getToken(MAG1Parser.T_or, 0);
        }



        public CondContext cond() {
            return getRuleContext(CondContext.class, 0);
        }



        public Condition1Context(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_condition1;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterCondition1(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitCondition1(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitCondition1(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final Condition1Context condition1() throws RecognitionException {
        Condition1Context _localctx = new Condition1Context(_ctx, getState());
        enterRule(_localctx, 16, RULE_condition1);
        try {
            setState(118);
            switch (getInterpreter().adaptivePredict(_input, 7, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(112);
                        match(T_or);
                        setState(113);
                        cond();
                        setState(114);
                        condition1();
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(116);
                        match(T_or);
                        setState(117);
                        cond();
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class CondContext extends ParserRuleContext {

        String type;



        public CContext c() {
            return getRuleContext(CContext.class, 0);
        }



        public Cond1Context cond1() {
            return getRuleContext(Cond1Context.class, 0);
        }



        public CondContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_cond;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterCond(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitCond(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitCond(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final CondContext cond() throws RecognitionException {
        CondContext _localctx = new CondContext(_ctx, getState());
        enterRule(_localctx, 18, RULE_cond);
        try {
            setState(124);
            switch (getInterpreter().adaptivePredict(_input, 8, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(120);
                        c();
                        setState(121);
                        cond1();
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(123);
                        c();
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class Cond1Context extends ParserRuleContext {

        String type;



        public CContext c() {
            return getRuleContext(CContext.class, 0);
        }



        public TerminalNode T_and() {
            return getToken(MAG1Parser.T_and, 0);
        }



        public Cond1Context cond1() {
            return getRuleContext(Cond1Context.class, 0);
        }



        public Cond1Context(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_cond1;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterCond1(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitCond1(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitCond1(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final Cond1Context cond1() throws RecognitionException {
        Cond1Context _localctx = new Cond1Context(_ctx, getState());
        enterRule(_localctx, 20, RULE_cond1);
        try {
            setState(132);
            switch (getInterpreter().adaptivePredict(_input, 9, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(126);
                        match(T_and);
                        setState(127);
                        c();
                        setState(128);
                        cond1();
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(130);
                        match(T_and);
                        setState(131);
                        c();
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class CContext extends ParserRuleContext {

        String type;



        public ConditionContext condition() {
            return getRuleContext(ConditionContext.class, 0);
        }



        public BbooleanContext bboolean() {
            return getRuleContext(BbooleanContext.class, 0);
        }



        public TerminalNode T_not() {
            return getToken(MAG1Parser.T_not, 0);
        }



        public TerminalNode T_pc() {
            return getToken(MAG1Parser.T_pc, 0);
        }



        public TerminalNode T_po() {
            return getToken(MAG1Parser.T_po, 0);
        }



        public CContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_c;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterC(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitC(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitC(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final CContext c() throws RecognitionException {
        CContext _localctx = new CContext(_ctx, getState());
        enterRule(_localctx, 22, RULE_c);
        try {
            setState(141);
            switch (getInterpreter().adaptivePredict(_input, 10, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(134);
                        match(T_po);
                        setState(135);
                        condition();
                        setState(136);
                        match(T_pc);
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(138);
                        match(T_not);
                        setState(139);
                        condition();
                    }
                    break;

                case 3:
                    enterOuterAlt(_localctx, 3);
                    {
                        setState(140);
                        bboolean();
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class BbooleanContext extends ParserRuleContext {

        String type;



        public List<EContext> e() {
            return getRuleContexts(EContext.class);
        }



        public RelopContext relop() {
            return getRuleContext(RelopContext.class, 0);
        }



        public EContext e(int i) {
            return getRuleContext(EContext.class, i);
        }



        public BbooleanContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_bboolean;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterBboolean(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitBboolean(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitBboolean(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final BbooleanContext bboolean() throws RecognitionException {
        BbooleanContext _localctx = new BbooleanContext(_ctx, getState());
        enterRule(_localctx, 24, RULE_bboolean);
        try {
            setState(150);
            switch (getInterpreter().adaptivePredict(_input, 11, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(143);
                        e();
                        setState(144);
                        relop();
                        setState(145);
                        e();
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(147);
                        e();
                    }
                    break;

                case 3:
                    enterOuterAlt(_localctx, 3);
                    {
                        setState(148);
                        match(2);
                    }
                    break;

                case 4:
                    enterOuterAlt(_localctx, 4);
                    {
                        setState(149);
                        match(1);
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class RelopContext extends ParserRuleContext {

        String type;



        public TerminalNode T_let() {
            return getToken(MAG1Parser.T_let, 0);
        }



        public TerminalNode T_et() {
            return getToken(MAG1Parser.T_et, 0);
        }



        public TerminalNode T_get() {
            return getToken(MAG1Parser.T_get, 0);
        }



        public TerminalNode T_gt() {
            return getToken(MAG1Parser.T_gt, 0);
        }



        public TerminalNode T_net() {
            return getToken(MAG1Parser.T_net, 0);
        }



        public TerminalNode T_lt() {
            return getToken(MAG1Parser.T_lt, 0);
        }



        public RelopContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_relop;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterRelop(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitRelop(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitRelop(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final RelopContext relop() throws RecognitionException {
        RelopContext _localctx = new RelopContext(_ctx, getState());
        enterRule(_localctx, 26, RULE_relop);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(152);
                _la = _input.LA(1);
                if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T_gt) | (1L << T_lt) | (1L << T_get) | (1L << T_let) | (1L << T_et) | (1L << T_net))) != 0))) {
                    _errHandler.recoverInline(this);
                }
                consume();
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class IfStContext extends ParserRuleContext {

        String type;



        public BlockStContext blockSt() {
            return getRuleContext(BlockStContext.class, 0);
        }



        public ConditionContext condition() {
            return getRuleContext(ConditionContext.class, 0);
        }



        public TerminalNode T_if() {
            return getToken(MAG1Parser.T_if, 0);
        }



        public ElsePartContext elsePart() {
            return getRuleContext(ElsePartContext.class, 0);
        }



        public IfStContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_ifSt;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterIfSt(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitIfSt(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitIfSt(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final IfStContext ifSt() throws RecognitionException {
        IfStContext _localctx = new IfStContext(_ctx, getState());
        enterRule(_localctx, 28, RULE_ifSt);
        try {
            setState(163);
            switch (getInterpreter().adaptivePredict(_input, 12, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(154);
                        match(T_if);
                        setState(155);
                        condition();
                        setState(156);
                        blockSt();
                        setState(157);
                        elsePart();
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(159);
                        match(T_if);
                        setState(160);
                        condition();
                        setState(161);
                        blockSt();
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ElsePartContext extends ParserRuleContext {

        String type;



        public TerminalNode T_else() {
            return getToken(MAG1Parser.T_else, 0);
        }



        public BlockStContext blockSt() {
            return getRuleContext(BlockStContext.class, 0);
        }



        public ElsePartContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_elsePart;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterElsePart(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitElsePart(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitElsePart(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final ElsePartContext elsePart() throws RecognitionException {
        ElsePartContext _localctx = new ElsePartContext(_ctx, getState());
        enterRule(_localctx, 30, RULE_elsePart);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(165);
                match(T_else);
                setState(166);
                blockSt();
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class StatementContext extends ParserRuleContext {

        String type;



        public StatementContext statement() {
            return getRuleContext(StatementContext.class, 0);
        }



        public SContext s() {
            return getRuleContext(SContext.class, 0);
        }



        public StatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_statement;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterStatement(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitStatement(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final StatementContext statement() throws RecognitionException {
        StatementContext _localctx = new StatementContext(_ctx, getState());
        enterRule(_localctx, 32, RULE_statement);
        try {
            setState(172);
            switch (getInterpreter().adaptivePredict(_input, 13, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(168);
                        s();
                        setState(169);
                        statement();
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(171);
                        s();
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class SContext extends ParserRuleContext {

        String type;



        public IOStContext iOSt() {
            return getRuleContext(IOStContext.class, 0);
        }



        public AssignmentStContext assignmentSt() {
            return getRuleContext(AssignmentStContext.class, 0);
        }



        public IfStContext ifSt() {
            return getRuleContext(IfStContext.class, 0);
        }



        public BlockStContext blockSt() {
            return getRuleContext(BlockStContext.class, 0);
        }



        public DeclarStContext declarSt() {
            return getRuleContext(DeclarStContext.class, 0);
        }



        public SContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_s;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterS(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitS(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitS(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final SContext s() throws RecognitionException {
        SContext _localctx = new SContext(_ctx, getState());
        enterRule(_localctx, 34, RULE_s);
        try {
            setState(180);
            switch (getInterpreter().adaptivePredict(_input, 14, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(174);
                        ifSt();
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(175);
                        blockSt();
                    }
                    break;

                case 3:
                    enterOuterAlt(_localctx, 3);
                    {
                        setState(176);
                        assignmentSt();
                    }
                    break;

                case 4:
                    enterOuterAlt(_localctx, 4);
                    {
                        setState(177);
                        ifSt();
                    }
                    break;

                case 5:
                    enterOuterAlt(_localctx, 5);
                    {
                        setState(178);
                        declarSt();
                    }
                    break;

                case 6:
                    enterOuterAlt(_localctx, 6);
                    {
                        setState(179);
                        iOSt();
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class BlockStContext extends ParserRuleContext {

        String type;



        public StatementContext statement() {
            return getRuleContext(StatementContext.class, 0);
        }



        public TerminalNode T_cbo() {
            return getToken(MAG1Parser.T_cbo, 0);
        }



        public TerminalNode T_cbc() {
            return getToken(MAG1Parser.T_cbc, 0);
        }



        public BlockStContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_blockSt;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterBlockSt(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitBlockSt(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitBlockSt(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final BlockStContext blockSt() throws RecognitionException {

        BlockStContext _localctx = new BlockStContext(_ctx, getState());
        enterRule(_localctx, 36, RULE_blockSt);
        try {
            setState(188);
            switch (getInterpreter().adaptivePredict(_input, 15, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(182);
                        match(T_cbo);
                        setState(183);
                        statement();
                        setState(184);
                        match(T_cbc);
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(186);
                        match(T_cbo);
                        setState(187);
                        match(T_cbc);
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class AssignmentStContext extends ParserRuleContext {

        String type;



        public EContext e() {
            return getRuleContext(EContext.class, 0);
        }



        public TerminalNode T_as() {
            return getToken(MAG1Parser.T_as, 0);
        }



        public TerminalNode T_ID() {
            return getToken(MAG1Parser.T_ID, 0);
        }



        public TerminalNode T_semicolon() {
            return getToken(MAG1Parser.T_semicolon, 0);
        }



        public AssignmentStContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_assignmentSt;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterAssignmentSt(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitAssignmentSt(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitAssignmentSt(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final AssignmentStContext assignmentSt() throws RecognitionException {
        AssignmentStContext _localctx = new AssignmentStContext(_ctx, getState());
        enterRule(_localctx, 38, RULE_assignmentSt);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(190);
                match(T_ID);
                setState(191);
                match(T_as);
                setState(192);
                e();
                setState(193);
                match(T_semicolon);
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class DeclarStContext extends ParserRuleContext {

        String type;



        public TerminalNode T_ID() {
            return getToken(MAG1Parser.T_ID, 0);
        }



        public TypeContext type() {
            return getRuleContext(TypeContext.class, 0);
        }



        public TerminalNode T_semicolon() {
            return getToken(MAG1Parser.T_semicolon, 0);
        }



        public DeclarStContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_declarSt;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterDeclarSt(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitDeclarSt(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitDeclarSt(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final DeclarStContext declarSt() throws RecognitionException {
        DeclarStContext _localctx = new DeclarStContext(_ctx, getState());
        enterRule(_localctx, 40, RULE_declarSt);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(195);
                type();
                setState(196);
                match(T_ID);
                setState(197);
                match(T_semicolon);
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class TypeContext extends ParserRuleContext {

        String type;



        public TerminalNode T_char() {
            return getToken(MAG1Parser.T_char, 0);
        }



        public TerminalNode T_float() {
            return getToken(MAG1Parser.T_float, 0);
        }



        public TerminalNode T_string() {
            return getToken(MAG1Parser.T_string, 0);
        }



        public TerminalNode T_int() {
            return getToken(MAG1Parser.T_int, 0);
        }



        public TypeContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_type;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterType(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitType(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitType(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final TypeContext type() throws RecognitionException {
        TypeContext _localctx = new TypeContext(_ctx, getState());
        enterRule(_localctx, 42, RULE_type);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(199);
                _la = _input.LA(1);
                if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T_int) | (1L << T_float) | (1L << T_char) | (1L << T_string))) != 0))) {
                    _errHandler.recoverInline(this);
                }
                consume();
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class IOStContext extends ParserRuleContext {

        String type;



        public InputStContext inputSt() {
            return getRuleContext(InputStContext.class, 0);
        }



        public OutputStContext outputSt() {
            return getRuleContext(OutputStContext.class, 0);
        }



        public IOStContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_iOSt;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterIOSt(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitIOSt(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitIOSt(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final IOStContext iOSt() throws RecognitionException {
        IOStContext _localctx = new IOStContext(_ctx, getState());
        enterRule(_localctx, 44, RULE_iOSt);
        try {
            setState(203);
            switch (getInterpreter().adaptivePredict(_input, 16, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(201);
                        inputSt();
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(202);
                        outputSt();
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class InputStContext extends ParserRuleContext {

        String type;



        public TerminalNode T_ID() {
            return getToken(MAG1Parser.T_ID, 0);
        }



        public TerminalNode T_semicolon() {
            return getToken(MAG1Parser.T_semicolon, 0);
        }



        public TerminalNode T_cin() {
            return getToken(MAG1Parser.T_cin, 0);
        }



        public InputStContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_inputSt;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterInputSt(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitInputSt(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitInputSt(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final InputStContext inputSt() throws RecognitionException {
        InputStContext _localctx = new InputStContext(_ctx, getState());
        enterRule(_localctx, 46, RULE_inputSt);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(205);
                match(T_cin);
                setState(206);
                match(T_ID);
                setState(207);
                match(T_semicolon);
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class OutputStContext extends ParserRuleContext {

        String type;



        public EContext e() {
            return getRuleContext(EContext.class, 0);
        }



        public TerminalNode T_cout() {
            return getToken(MAG1Parser.T_cout, 0);
        }



        public TerminalNode T_semicolon() {
            return getToken(MAG1Parser.T_semicolon, 0);
        }



        public TerminalNode T_stringvar() {
            return getToken(MAG1Parser.T_stringvar, 0);
        }



        public OutputStContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }



        @Override
        public int getRuleIndex() {
            return RULE_outputSt;
        }



        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).enterOutputSt(this);
        }



        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MAG1Listener)
                ((MAG1Listener) listener).exitOutputSt(this);
        }



        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MAG1Visitor)
                return ((MAG1Visitor<? extends T>) visitor).visitOutputSt(this);
            else
                return visitor.visitChildren(this);
        }
    }



    public final OutputStContext outputSt() throws RecognitionException {
        OutputStContext _localctx = new OutputStContext(_ctx, getState());
        enterRule(_localctx, 48, RULE_outputSt);
        try {
            setState(216);
            switch (getInterpreter().adaptivePredict(_input, 17, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                    {
                        setState(209);
                        match(T_cout);
                        setState(210);
                        match(T_stringvar);
                        setState(211);
                        match(T_semicolon);
                    }
                    break;

                case 2:
                    enterOuterAlt(_localctx, 2);
                    {
                        setState(212);
                        match(T_cout);
                        setState(213);
                        e();
                        setState(214);
                        match(T_semicolon);
                    }
                    break;
            }
        }
        catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static final String _serializedATN =
                                                      "\2\3+\u00dd\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4" +
                                                              "\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20" +
                                                              "\4\21\t\21\4\22\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27" +
                                                              "\4\30\t\30\4\31\t\31\4\32\t\32\3\2\7\2\66\n\2\f\2\16\29\13\2\3\3\3\3\3" +
                                                              "\3\3\3\5\3?\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4M\n" +
                                                              "\4\3\5\3\5\3\5\3\5\5\5S\n\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3" +
                                                              "\6\3\6\5\6a\n\6\3\7\3\7\3\7\3\7\3\7\3\7\5\7i\n\7\3\b\3\b\3\t\3\t\3\t\3" +
                                                              "\t\5\tq\n\t\3\n\3\n\3\n\3\n\3\n\3\n\5\ny\n\n\3\13\3\13\3\13\3\13\5\13" +
                                                              "\177\n\13\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u0087\n\f\3\r\3\r\3\r\3\r\3\r\3" +
                                                              "\r\3\r\5\r\u0090\n\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u0099\n\16" +
                                                              "\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\5\20\u00a6\n\20" +
                                                              "\3\21\3\21\3\21\3\22\3\22\3\22\3\22\5\22\u00af\n\22\3\23\3\23\3\23\3\23" +
                                                              "\3\23\3\23\5\23\u00b7\n\23\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u00bf\n" +
                                                              "\24\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\27\3\27\3\30\3\30\5" +
                                                              "\30\u00ce\n\30\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32" +
                                                              "\5\32\u00db\n\32\3\32\2\33\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$" +
                                                              "&(*,.\60\62\2\5\4\36\36  \3\n\17\6\35\35\37\37!!##\u00e1\2\67\3\2\2\2" +
                                                              "\4>\3\2\2\2\6L\3\2\2\2\bR\3\2\2\2\n`\3\2\2\2\fh\3\2\2\2\16j\3\2\2\2\20" +
                                                              "p\3\2\2\2\22x\3\2\2\2\24~\3\2\2\2\26\u0086\3\2\2\2\30\u008f\3\2\2\2\32" +
                                                              "\u0098\3\2\2\2\34\u009a\3\2\2\2\36\u00a5\3\2\2\2 \u00a7\3\2\2\2\"\u00ae" +
                                                              "\3\2\2\2$\u00b6\3\2\2\2&\u00be\3\2\2\2(\u00c0\3\2\2\2*\u00c5\3\2\2\2," +
                                                              "\u00c9\3\2\2\2.\u00cd\3\2\2\2\60\u00cf\3\2\2\2\62\u00da\3\2\2\2\64\66" +
                                                              "\5\"\22\2\65\64\3\2\2\2\669\3\2\2\2\67\65\3\2\2\2\678\3\2\2\28\3\3\2\2" +
                                                              "\29\67\3\2\2\2:;\5\b\5\2;<\5\6\4\2<?\3\2\2\2=?\5\b\5\2>:\3\2\2\2>=\3\2" +
                                                              "\2\2?\5\3\2\2\2@A\7\5\2\2AB\5\b\5\2BC\5\6\4\2CM\3\2\2\2DE\7\6\2\2EF\5" +
                                                              "\b\5\2FG\5\6\4\2GM\3\2\2\2HI\7\6\2\2IM\5\b\5\2JK\7\5\2\2KM\5\b\5\2L@\3" +
                                                              "\2\2\2LD\3\2\2\2LH\3\2\2\2LJ\3\2\2\2M\7\3\2\2\2NO\5\f\7\2OP\5\n\6\2PS" +
                                                              "\3\2\2\2QS\5\f\7\2RN\3\2\2\2RQ\3\2\2\2S\t\3\2\2\2TU\7\7\2\2UV\5\f\7\2" +
                                                              "VW\5\n\6\2Wa\3\2\2\2XY\7\b\2\2YZ\5\f\7\2Z[\5\n\6\2[a\3\2\2\2\\]\7\7\2" +
                                                              "\2]a\5\f\7\2^_\7\t\2\2_a\5\f\7\2`T\3\2\2\2`X\3\2\2\2`\\\3\2\2\2`^\3\2" +
                                                              "\2\2a\13\3\2\2\2bc\7\27\2\2cd\5\4\3\2de\7\30\2\2ei\3\2\2\2fi\7+\2\2gi" +
                                                              "\5\16\b\2hb\3\2\2\2hf\3\2\2\2hg\3\2\2\2i\r\3\2\2\2jk\t\2\2\2k\17\3\2\2" +
                                                              "\2lm\5\24\13\2mn\5\22\n\2nq\3\2\2\2oq\5\24\13\2pl\3\2\2\2po\3\2\2\2q\21" +
                                                              "\3\2\2\2rs\7\22\2\2st\5\24\13\2tu\5\22\n\2uy\3\2\2\2vw\7\22\2\2wy\5\24" +
                                                              "\13\2xr\3\2\2\2xv\3\2\2\2y\23\3\2\2\2z{\5\30\r\2{|\5\26\f\2|\177\3\2\2" +
                                                              "\2}\177\5\30\r\2~z\3\2\2\2~}\3\2\2\2\177\25\3\2\2\2\u0080\u0081\7\21\2" +
                                                              "\2\u0081\u0082\5\30\r\2\u0082\u0083\5\26\f\2\u0083\u0087\3\2\2\2\u0084" +
                                                              "\u0085\7\21\2\2\u0085\u0087\5\30\r\2\u0086\u0080\3\2\2\2\u0086\u0084\3" +
                                                              "\2\2\2\u0087\27\3\2\2\2\u0088\u0089\7\27\2\2\u0089\u008a\5\20\t\2\u008a" +
                                                              "\u008b\7\30\2\2\u008b\u0090\3\2\2\2\u008c\u008d\7\23\2\2\u008d\u0090\5" +
                                                              "\20\t\2\u008e\u0090\5\32\16\2\u008f\u0088\3\2\2\2\u008f\u008c\3\2\2\2" +
                                                              "\u008f\u008e\3\2\2\2\u0090\31\3\2\2\2\u0091\u0092\5\4\3\2\u0092\u0093" +
                                                              "\5\34\17\2\u0093\u0094\5\4\3\2\u0094\u0099\3\2\2\2\u0095\u0099\5\4\3\2" +
                                                              "\u0096\u0099\7\4\2\2\u0097\u0099\7\3\2\2\u0098\u0091\3\2\2\2\u0098\u0095" +
                                                              "\3\2\2\2\u0098\u0096\3\2\2\2\u0098\u0097\3\2\2\2\u0099\33\3\2\2\2\u009a" +
                                                              "\u009b\t\3\2\2\u009b\35\3\2\2\2\u009c\u009d\7\24\2\2\u009d\u009e\5\20" +
                                                              "\t\2\u009e\u009f\5&\24\2\u009f\u00a0\5 \21\2\u00a0\u00a6\3\2\2\2\u00a1" +
                                                              "\u00a2\7\24\2\2\u00a2\u00a3\5\20\t\2\u00a3\u00a4\5&\24\2\u00a4\u00a6\3" +
                                                              "\2\2\2\u00a5\u009c\3\2\2\2\u00a5\u00a1\3\2\2\2\u00a6\37\3\2\2\2\u00a7" +
                                                              "\u00a8\7\25\2\2\u00a8\u00a9\5&\24\2\u00a9!\3\2\2\2\u00aa\u00ab\5$\23\2" +
                                                              "\u00ab\u00ac\5\"\22\2\u00ac\u00af\3\2\2\2\u00ad\u00af\5$\23\2\u00ae\u00aa" +
                                                              "\3\2\2\2\u00ae\u00ad\3\2\2\2\u00af#\3\2\2\2\u00b0\u00b7\5\36\20\2\u00b1" +
                                                              "\u00b7\5&\24\2\u00b2\u00b7\5(\25\2\u00b3\u00b7\5\36\20\2\u00b4\u00b7\5" +
                                                              "*\26\2\u00b5\u00b7\5.\30\2\u00b6\u00b0\3\2\2\2\u00b6\u00b1\3\2\2\2\u00b6" +
                                                              "\u00b2\3\2\2\2\u00b6\u00b3\3\2\2\2\u00b6\u00b4\3\2\2\2\u00b6\u00b5\3\2" +
                                                              "\2\2\u00b7%\3\2\2\2\u00b8\u00b9\7\31\2\2\u00b9\u00ba\5\"\22\2\u00ba\u00bb" +
                                                              "\7\32\2\2\u00bb\u00bf\3\2\2\2\u00bc\u00bd\7\31\2\2\u00bd\u00bf\7\32\2" +
                                                              "\2\u00be\u00b8\3\2\2\2\u00be\u00bc\3\2\2\2\u00bf\'\3\2\2\2\u00c0\u00c1" +
                                                              "\7+\2\2\u00c1\u00c2\7\20\2\2\u00c2\u00c3\5\4\3\2\u00c3\u00c4\7&\2\2\u00c4" +
                                                              ")\3\2\2\2\u00c5\u00c6\5,\27\2\u00c6\u00c7\7+\2\2\u00c7\u00c8\7&\2\2\u00c8" +
                                                              "+\3\2\2\2\u00c9\u00ca\t\4\2\2\u00ca-\3\2\2\2\u00cb\u00ce\5\60\31\2\u00cc" +
                                                              "\u00ce\5\62\32\2\u00cd\u00cb\3\2\2\2\u00cd\u00cc\3\2\2\2\u00ce/\3\2\2" +
                                                              "\2\u00cf\u00d0\7\33\2\2\u00d0\u00d1\7+\2\2\u00d1\u00d2\7&\2\2\u00d2\61" +
                                                              "\3\2\2\2\u00d3\u00d4\7\34\2\2\u00d4\u00d5\7$\2\2\u00d5\u00db\7&\2\2\u00d6" +
                                                              "\u00d7\7\34\2\2\u00d7\u00d8\5\4\3\2\u00d8\u00d9\7&\2\2\u00d9\u00db\3\2" +
                                                              "\2\2\u00da\u00d3\3\2\2\2\u00da\u00d6\3\2\2\2\u00db\63\3\2\2\2\24\67>L" +
                                                              "R`hpx~\u0086\u008f\u0098\u00a5\u00ae\u00b6\u00be\u00cd\u00da";
    public static final ATN    _ATN           =
                                                      ATNSimulator.deserialize(_serializedATN.toCharArray());
    static {
        _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
    }
}