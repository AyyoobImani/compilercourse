// Define a grammar called Hello
grammar MAG1;

@parser::members {
 public int blockNumber=0;
 public int blockDepth=0;
}

//s : (T_plus | T_min | T_po | T_pc | T_intvar )*;
start : (statement)*;
//e : e T_plus t | e T_min t | t;
e : t e1 | t;
e1 : T_plus t e1 | T_min t e1 | T_min t | T_plus t;
//t : t T_star f | t T_slash f | t T_mode f | f;
t : f t1 | f;
t1 : T_star f t1| T_slash f t1| T_star f | T_mode f ;
f : T_po e T_pc | T_ID | no;
no: T_intvar | T_floatvar ;

condition : condition T_or cond | cond;
cond : cond T_and c | c;
c : T_po condition T_pc | T_not condition  | bboolean;
bboolean : e relop e | e | 'true' | 'false';
relop : T_gt |T_lt |T_get |T_let |T_et |T_net {System.out.print($T_net.line();)} ;


ifSt : T_if condition  blockSt  elsePart|T_if condition blockSt;
elsePart : T_else blockSt;

//statement: statement  s | s ;
statement : s statement | s;
//statement1 : T_semicolon s statement1 | T_semicolon s;
s : ifSt |blockSt | assignmentSt | ifSt | declarSt | iOSt ;

blockSt : T_cbo statement T_cbc|T_cbo T_cbc;

assignmentSt : T_ID T_as e T_semicolon;

declarSt : type T_ID T_semicolon;
type:T_int | T_float | T_char | T_string;
/*
iOSt: iO e T_semicolon | iO T_string T_semicolon;
iO : T_cin | T_cout;
*/
iOSt: inputSt | outputSt;
inputSt : T_cin T_ID T_semicolon;
outputSt :  T_cout T_stringvar T_semicolon | T_cout e T_semicolon ;


//r : (T_plus|T_min|T_star|T_slash|T_mode|T_gt|T_let|T_lt|T_get|T_et|T_net|T_as|T_and|T_or|T_not|T_if|T_po|T_pc|T_cbo|T_cbc|T_cin|T_cout|T_int|T_intvar|T_float|T_floatvar|T_char|T_charvar|T_string|T_stringvar|T_ID|T_semicolon|T_throw|T_try|T_catch)*;
/*m : (T_plus {System.out.println("token T_plus:"+$T_plus.text+" line:"+$T_plus.line+" col:"+$T_plus.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_min {System.out.println("token T_min:"+$T_min.text+" line:"+$T_min.line+" col:"+$T_min.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_star {System.out.println("token T_star:"+$T_star.text+" line:"+$T_star.line+" col:"+$T_star.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_slash {System.out.println("token T_slash:"+$T_slash.text+" line:"+$T_slash.line+" col:"+$T_slash.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_mode {System.out.println("token T_mode:"+$T_mode.text+" line:"+$T_mode.line+" col:"+$T_mode.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_gt {System.out.println("token T_gt:"+$T_gt.text+" line:"+$T_gt.line+" col:"+$T_gt.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_let {System.out.println("token T_let:"+$T_let.text+" line:"+$T_let.line+" col:"+$T_let.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_lt {System.out.println("token T_lt:"+$T_lt.text+" line:"+$T_lt.line+" col:"+$T_lt.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_get {System.out.println("token T_get:"+$T_get.text+" line:"+$T_get.line+" col:"+$T_get.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_et {System.out.println("token T_et:"+$T_et.text+" line:"+$T_et.line+" col:"+$T_et.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_net {System.out.println("token T_net:"+$T_net.text+" line:"+$T_net.line+" col:"+$T_net.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_as {System.out.println("token T_as:"+$T_as.text+" line:"+$T_as.line+" col:"+$T_as.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_and {System.out.println("token T_and:"+$T_and.text+" line:"+$T_and.line+" col:"+$T_and.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_or {System.out.println("token T_or:"+$T_or.text+" line:"+$T_or.line+" col:"+$T_or.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_not {System.out.println("token T_not:"+$T_not.text+" line:"+$T_not.line+" col:"+$T_not.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_if {System.out.println("token T_if:"+$T_if.text+" line:"+$T_if.line+" col:"+$T_if.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_po {System.out.println("token T_po:"+$T_po.text+" line:"+$T_po.line+" col:"+$T_po.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);blockNumber++;blockDepth++;}|
    T_pc {System.out.println("token T_pc:"+$T_pc.text+" line:"+$T_pc.line+" col:"+$T_pc.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);blockDepth--;}|
    T_cbo {System.out.println("token T_cbo:"+$T_cbo.text+" line:"+$T_cbo.line+" col:"+$T_cbo.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_cbc {System.out.println("token T_cbc:"+$T_cbc.text+" line:"+$T_cbc.line+" col:"+$T_cbc.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|   
    T_cin {System.out.println("token T_cin:"+$T_cin.text+" line:"+$T_cin.line+" col:"+$T_cin.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|   
    T_cout {System.out.println("token T_cout:"+$T_cout.text+" line:"+$T_cout.line+" col:"+$T_cout.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|
    T_int {System.out.println("token T_int:"+$T_int.text+" line:"+$T_int.line+" col:"+$T_int.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|   
    T_intvar {System.out.println("token T_intvar:"+$T_intvar.text+" line:"+$T_intvar.line+" col:"+$T_intvar.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|   
    T_float {System.out.println("token T_float:"+$T_float.text+" line:"+$T_float.line+" col:"+$T_float.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|   
    T_floatvar {System.out.println("token T_floatvar:"+$T_floatvar.text+" line:"+$T_floatvar.line+" col:"+$T_floatvar.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|   
    T_char {System.out.println("token T_char:"+$T_char.text+" line:"+$T_char.line+" col:"+$T_char.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|   
    T_charvar {System.out.println("token T_charvar:"+$T_charvar.text+" line:"+$T_charvar.line+" col:"+$T_charvar.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|   
    T_string {System.out.println("token T_string:"+$T_string.text+" line:"+$T_string.line+" col:"+$T_string.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|   
    T_stringvar {System.out.println("token T_stringvar:"+$T_stringvar.text+" line:"+$T_stringvar.line+" col:"+$T_stringvar.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|   
    T_ID {System.out.println("token T_ID:"+$T_ID.text+" line:"+$T_ID.line+" col:"+$T_ID.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|   
    T_semicolon {System.out.println("token T_semicolon:"+$T_semicolon.text+" line:"+$T_semicolon.line+" col:"+$T_semicolon.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|   
    T_throw {System.out.println("token T_throw:"+$T_throw.text+" line:"+$T_throw.line+" col:"+$T_throw.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|   
    T_try {System.out.println("token T_try:"+$T_try.text+" line:"+$T_try.line+" col:"+$T_try.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);}|   

    T_catch {System.out.println("token T_catch:"+$T_catch.text+" line:"+$T_catch.line+" col:"+$T_catch.pos+" blocknumber:"+blockNumber+" blockDepth:"+blockDepth);})*;
*/
T_plus : '+';
T_min : '-';
T_star : '*';
T_slash : '/';
T_mode: '%';
T_gt : '>';
T_lt : '<';
T_get : '>=';
T_let : '<=';
T_et : '==';
T_net : '!=';
T_as : '=';
T_and : '&&';
T_or : '||';
T_not : '!';
T_if : 'if';
T_else : 'else';
T_elif : 'elif';
T_po : '(';
T_pc : ')';
T_cbo : '{';
T_cbc : '}';
T_cin :'>>';
T_cout : '<<';
T_int : 'int';
T_intvar : ('0'..'9')+;
T_float : 'float';
T_floatvar : ('0'..'9')+'.'('0'..'9')+;
T_char : 'char';
T_charvar : '\''('a'..'z'|'A'..'Z')'\'';
T_string : 'string';
T_stringvar : '"'('\\"'|.)*?'"';

T_comment : '/*'.*?'*/' -> skip;
T_semicolon : ';';
T_throw : 'throw';
T_try : 'try';
T_catch: 'catch';
T_whitespace : [ \n\t\r]+ -> skip ;

T_ID : ('_'|'a'..'z'|'A'..'Z')('_'|'a'..'z'|'A'..'Z'|'0'..'9')*;


//T_true : "true";
//T_false : 'false';

/*

*/