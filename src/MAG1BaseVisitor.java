// Generated from C:\Enter here\documents\courses\91-92_2\Compiler\project\vandermonde\MAG1.g4 by ANTLR 4.0

import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

public class MAG1BaseVisitor<T> extends AbstractParseTreeVisitor<T> implements MAG1Visitor<T> {

    @Override
    public T visitAssignmentSt(MAG1Parser.AssignmentStContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitIOSt(MAG1Parser.IOStContext ctx) {
        return visitChildren(ctx);

    }



    @Override
    public T visitT1(MAG1Parser.T1Context ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitNo(MAG1Parser.NoContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitRelop(MAG1Parser.RelopContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitType(MAG1Parser.TypeContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitCond(MAG1Parser.CondContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitCond1(MAG1Parser.Cond1Context ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitDeclarSt(MAG1Parser.DeclarStContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitBboolean(MAG1Parser.BbooleanContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitF(MAG1Parser.FContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitE(MAG1Parser.EContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitC(MAG1Parser.CContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitCondition(MAG1Parser.ConditionContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitBlockSt(MAG1Parser.BlockStContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitElsePart(MAG1Parser.ElsePartContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitStatement(MAG1Parser.StatementContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitE1(MAG1Parser.E1Context ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitCondition1(MAG1Parser.Condition1Context ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitT(MAG1Parser.TContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitIfSt(MAG1Parser.IfStContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitS(MAG1Parser.SContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitStart(MAG1Parser.StartContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitInputSt(MAG1Parser.InputStContext ctx) {
        return visitChildren(ctx);
    }



    @Override
    public T visitOutputSt(MAG1Parser.OutputStContext ctx) {
        return visitChildren(ctx);
    }
}