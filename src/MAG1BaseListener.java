// Generated from C:\Enter here\documents\courses\91-92_2\Compiler\project\vandermonde\MAG1.g4 by ANTLR 4.0

import java.util.ArrayList;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

public class MAG1BaseListener implements MAG1Listener {

    private String findVariable(String inputName)
    {
        Scope resScope = null;

        for (int i = 0; i < allScope.size(); i++)
        {
            if (allScope.get(i).depth == scopeDepth)

            {

                resScope = allScope.get(i);
            }
        }

        boolean a = true;
        while (a)

        {

            for (int j = 0; j < resScope.data.size(); j++)
            {

                if (resScope.data.get(j).name.compareTo(inputName) == 0)
                {

                    return resScope.data.get(j).value;
                }
            }

            if (resScope.father == null)
                return null;
            resScope = resScope.father;

        }

        return null;

    }

    int   scopeId;
    int   scopeDepth;

    Scope scopes;



    public MAG1BaseListener() {

        scopes = new Scope();

        scopes.father = null;
        scopes.depth = 0;
        scopes.id = 0;

        scopeId = 0;
        scopeDepth = 0;

        allScope.add(scopes);
    }

    public class type {

        String name;
        String value;
        String content;
    }

    public class Scope {

        Scope father;
        int   id;
        int   depth;



        public Scope() {

            data = new ArrayList<type>();
        }

        ArrayList<type> data;
    }

    ArrayList<Scope> allScope = new ArrayList<Scope>();



    @Override
    public void enterAssignmentSt(MAG1Parser.AssignmentStContext ctx) {

    }



    @Override
    public void exitAssignmentSt(MAG1Parser.AssignmentStContext ctx) {
        System.out.println("found assignment:" + ((TerminalNode) ctx.getChild(0)).getText());
        //MAG1Parser.AssignmentStContext ss =  (MAG1Parser.AssignmentStContext) ctx.getChild(0);

        String leftSideT = findVariable(((TerminalNode) ctx.getChild(0)).getText());

        System.out.println(((MAG1Parser.EContext) ctx.getChild(2)).type);
        System.out.println("scope : " + scopeDepth);
        if (leftSideT == null || leftSideT != ((MAG1Parser.EContext) ctx.getChild(2)).type)
        {
            System.out.println("error type checking");
            System.exit(0);
        }

    }



    @Override
    public void enterIOSt(MAG1Parser.IOStContext ctx) {}



    @Override
    public void exitIOSt(MAG1Parser.IOStContext ctx) {

        //System.out.println("found io statement");
        //System.out.println(ctx.toStringTree());
        // ctx.type();
    }



    @Override
    public void enterT1(MAG1Parser.T1Context ctx) {}



    @Override
    public void exitT1(MAG1Parser.T1Context ctx) {}



    @Override
    public void enterNo(MAG1Parser.NoContext ctx) {}



    @Override
    public void exitNo(MAG1Parser.NoContext ctx) {
        if (ctx.T_floatvar() != null)
        {
            ctx.type = "T_float";
        }

        else
        {
            ctx.type = "T_int";
        }

    }



    @Override
    public void enterRelop(MAG1Parser.RelopContext ctx) {}



    @Override
    public void exitRelop(MAG1Parser.RelopContext ctx) {}



    @Override
    public void enterType(MAG1Parser.TypeContext ctx) {

    }



    @Override
    public void exitType(MAG1Parser.TypeContext ctx) {
        if (ctx.T_char() != null)
            ctx.type = "T_char";
        else if (ctx.T_float() != null)
            ctx.type = "T_float";
        else if (ctx.T_int() != null)
            ctx.type = "T_int";
        else
            ctx.type = "T_string";

        System.out.println("new type: " + ctx.type);
    }



    @Override
    public void enterCond(MAG1Parser.CondContext ctx) {}



    @Override
    public void exitCond(MAG1Parser.CondContext ctx) {}



    @Override
    public void enterCond1(MAG1Parser.Cond1Context ctx) {}



    @Override
    public void exitCond1(MAG1Parser.Cond1Context ctx) {}



    @Override
    public void enterDeclarSt(MAG1Parser.DeclarStContext ctx) {}



    @Override
    public void exitDeclarSt(MAG1Parser.DeclarStContext ctx) {
        System.out.println("found declstatement: ");
        // System.out.print(ctx.getChildCount());
        //for (int i = 0; i < ctx.getChildCount(); i++)
        //   System.out.println(ctx.getChild(i).getText());
        //System.out.print(ctx.type()); //System.out.println(ctx.getTokens(0)
        for (int i = 0; i < allScope.size(); i++)
        {
            if (allScope.get(i).depth == scopeDepth)
            {
                type ntype = new type();
                ntype.value = ((MAG1Parser.TypeContext) ctx.getChild(0)).type;
                ntype.name = ((TerminalNode) ctx.getChild(1)).getText();
                ntype.value = null;
                allScope.get(i).data.add(ntype);
            }
        }

        ctx.type = ((MAG1Parser.TypeContext) ctx.getChild(0)).type;
        System.out.println(ctx.type);
    }



    @Override
    public void enterBboolean(MAG1Parser.BbooleanContext ctx) {}



    @Override
    public void exitBboolean(MAG1Parser.BbooleanContext ctx) {}



    @Override
    public void enterF(MAG1Parser.FContext ctx) {}



    @Override
    public void exitF(MAG1Parser.FContext ctx) {

        if (ctx.no() != null)
        {
            ctx.type = ((ctx.no()).type);
        }
    }



    @Override
    public void enterE(MAG1Parser.EContext ctx) {}



    @Override
    public void exitE(MAG1Parser.EContext ctx) {
        if (ctx.t() != null)
        {
            ctx.type = ctx.t().type;
        }
    }



    @Override
    public void enterC(MAG1Parser.CContext ctx) {}



    @Override
    public void exitC(MAG1Parser.CContext ctx) {}



    @Override
    public void enterCondition(MAG1Parser.ConditionContext ctx) {}



    @Override
    public void exitCondition(MAG1Parser.ConditionContext ctx) {}



    @Override
    public void enterBlockSt(MAG1Parser.BlockStContext ctx) {}



    @Override
    public void exitBlockSt(MAG1Parser.BlockStContext ctx) {
        System.out.println("found block");
    }



    @Override
    public void enterElsePart(MAG1Parser.ElsePartContext ctx) {}



    @Override
    public void exitElsePart(MAG1Parser.ElsePartContext ctx) {}



    @Override
    public void enterStatement(MAG1Parser.StatementContext ctx) {}



    @Override
    public void exitStatement(MAG1Parser.StatementContext ctx) {}



    @Override
    public void enterE1(MAG1Parser.E1Context ctx) {}



    @Override
    public void exitE1(MAG1Parser.E1Context ctx) {}



    @Override
    public void enterCondition1(MAG1Parser.Condition1Context ctx) {}



    @Override
    public void exitCondition1(MAG1Parser.Condition1Context ctx) {

    }



    @Override
    public void enterT(MAG1Parser.TContext ctx) {}



    @Override
    public void exitT(MAG1Parser.TContext ctx) {
        if (ctx.f() != null)
        {
            ctx.type = ctx.f().type;
        }
    }



    @Override
    public void enterIfSt(MAG1Parser.IfStContext ctx) {

        System.out.println("found if statement");

    }



    @Override
    public void exitIfSt(MAG1Parser.IfStContext ctx) {}



    @Override
    public void enterS(MAG1Parser.SContext ctx) {}



    @Override
    public void exitS(MAG1Parser.SContext ctx) {}



    @Override
    public void enterStart(MAG1Parser.StartContext ctx) {}



    @Override
    public void exitStart(MAG1Parser.StartContext ctx) {
        // System.out.println("found start ");
    }



    @Override
    public void enterInputSt(MAG1Parser.InputStContext ctx) {}



    @Override
    public void exitInputSt(MAG1Parser.InputStContext ctx) {}



    @Override
    public void enterOutputSt(MAG1Parser.OutputStContext ctx) {}



    @Override
    public void exitOutputSt(MAG1Parser.OutputStContext ctx) {

    }



    @Override
    public void enterEveryRule(ParserRuleContext ctx) {}



    @Override
    public void exitEveryRule(ParserRuleContext ctx) {

    }



    @Override
    public void visitTerminal(TerminalNode node)
    {

        if (node.getText().compareTo("{") == 0)
        {

            scopeId += 1;
            scopeDepth += 1;

            Scope nscope = new Scope();

            for (int i = 0; i < allScope.size(); i++)
            {
                if (allScope.get(i).depth == (scopeDepth - 1))
                    nscope.father = allScope.get(i);
            }

            nscope.depth = scopeDepth;
            nscope.id = scopeId;
            allScope.add(nscope);
        }

        if (node.getText().compareTo("}") == 0)
        {

            for (int i = 0; i < scopeDepth; i++)
            {
                if (allScope.get(i).depth == scopeDepth)
                    allScope.remove(i);
            }
            scopeDepth--;
        }

    }



    @Override
    public void visitErrorNode(ErrorNode node) {}
}