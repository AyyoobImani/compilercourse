// Generated from C:\Enter here\documents\courses\91-92_2\Compiler\project\vandermonde\MAG1.g4 by ANTLR 4.0

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

public interface MAG1Visitor<T> extends ParseTreeVisitor<T> {

    T visitAssignmentSt(MAG1Parser.AssignmentStContext ctx);



    T visitIOSt(MAG1Parser.IOStContext ctx);



    T visitT1(MAG1Parser.T1Context ctx);



    T visitNo(MAG1Parser.NoContext ctx);



    T visitRelop(MAG1Parser.RelopContext ctx);



    T visitType(MAG1Parser.TypeContext ctx);



    T visitCond(MAG1Parser.CondContext ctx);



    T visitCond1(MAG1Parser.Cond1Context ctx);



    T visitDeclarSt(MAG1Parser.DeclarStContext ctx);



    T visitBboolean(MAG1Parser.BbooleanContext ctx);



    T visitF(MAG1Parser.FContext ctx);



    T visitE(MAG1Parser.EContext ctx);



    T visitC(MAG1Parser.CContext ctx);



    T visitCondition(MAG1Parser.ConditionContext ctx);



    T visitBlockSt(MAG1Parser.BlockStContext ctx);



    T visitElsePart(MAG1Parser.ElsePartContext ctx);



    T visitStatement(MAG1Parser.StatementContext ctx);



    T visitE1(MAG1Parser.E1Context ctx);



    T visitCondition1(MAG1Parser.Condition1Context ctx);



    T visitT(MAG1Parser.TContext ctx);



    T visitIfSt(MAG1Parser.IfStContext ctx);



    T visitS(MAG1Parser.SContext ctx);



    T visitStart(MAG1Parser.StartContext ctx);



    T visitInputSt(MAG1Parser.InputStContext ctx);



    T visitOutputSt(MAG1Parser.OutputStContext ctx);
}